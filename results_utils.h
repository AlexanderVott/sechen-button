#include "EEPROM_utils.h"

struct result_item_t {
  State state;
  unsigned long ticks;
  bool win;
  bool final;
} resultItem = {neutral, 0, false, false};

result_item_t oldResult;
int currentAddress = 0;
int readableAddress = 0;

void WriteResult(result_item_t item) {
  // переписываем предыдущий результат, как не конечный.
  if (oldResult.final == true) // на случай нового запуска
  {
    oldResult.final = false;
    int address = CheckMinusMemory(currentAddress, sizeof(item));
    EEPROM_WriteAnything(address, oldResult);
  }
  // пишем новые данные
  item.final = true;
  currentAddress = CheckOverflowMemory(currentAddress, sizeof(item));
  Serial.println("WriteToMemory: " + (String)currentAddress);
  currentAddress += EEPROM_WriteAnything(currentAddress, item);
  oldResult = item;
}

result_item_t ReadResultNext() {
  result_item_t readItem;
  EEPROM_ReadAnything(readableAddress, readItem);
  if (readItem.final)
    readableAddress = 0;
  else
    readableAddress = sizeof(result_item_t);
  return readItem;
}

result_item_t ReadResultFirst() {
  readableAddress = 0;
  return ReadResultNext();
}
