#include <IRremote.h>

#include "display16x2.h"
#include "global.h"
#include "results_utils.h"
#include "timer_logic.h"
#include "editor_logic.h"

void setup() {
  Serial.begin(9600);

  LcdInit(true);

  GlobalInit();
  TimerLogicInit();
  EditorLogicInit();
}

void loop() {
  GlobalLoop();
  if (isEditMode)
    EditorLogicLoop();
  else
    TimerLogicLoop();

  if (isIRRes) {
    Serial.println(irRes.value, HEX);
    if (irRes.value == 0xff906f) {
      LcdClear();
      isEditMode = !isEditMode;
      Serial.println("EditMode: " + (String)isEditMode);
      Serial.println("MaxTime: " + (String)maxTime);
      if (!isEditMode)
        PrintStatus(currentState);
    }
  }
}
