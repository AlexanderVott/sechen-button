#define DEBUG = true;

typedef enum {
  neutral = 0,
  one = 1,
  two = 2
} State;
byte StateHigh = 2;

State currentState = neutral;
String statusNames[3] = { "idle", "one", "two" };

typedef enum {
  timeedit = 0,
  colorone = 1,
  colortwo = 2
} MenuState;
byte MenuStateHigh = 2;

MenuState menuState = timeedit;
String menuNames[3] = { "timeedit", "color one", "color two" };

typedef enum {
  red = 0,
  green = 1,
  blue = 2,
  yellow = 3,
  orange = 4,
  white = 5  
} Color;
byte ColorHigh = 5;

Color teams[2] = { red, green };
String colors[6] = { "red", "green", "blue", "yellow", "orange", "white" };

String ColorToStr(Color color) {
  return colors[(int)color];
}

bool isFirst = true;
bool isEditMode = false;

unsigned long currentTime;
unsigned long loopTime;
unsigned long maxTime = 15000;//900000;

IRrecv irrecv(6);
decode_results irRes;
bool isIRRes = false;

void PrintStatus(int stateValue) {
  String value = "status: ";// + statusNames[stateValue];
  switch ((State)stateValue)
  {
    case one:
    case two:
      value = value + colors[(int)teams[stateValue - 1]];
      break;
    default:
      value = value + statusNames[stateValue];
      break;
  }
  if (stateValue == (int)neutral)
  {
    
  }
  LcdClearLine(0);
  LcdPrint(0, 0, value);
  #ifdef DEBUG
    Serial.println(value);
  #endif
}

char * TimeToString(unsigned long t)
{
  t = t / 1000;
  static char str[12];
  long h = t / 3600;
  t = t % 3600;
  int m = t / 60;

  int s = t % 60;
  sprintf(str, "%04ld:%02d:%02d", h, m, s);
  return str;
}

char * TimeEditToString(unsigned long t)
{
  t = t / 1000;
  static char str[12];
  long h = t / 3600;
  t = t % 3600;
  int m = t / 60;

  int s = t % 60;
  sprintf(str, "%04ld:%02d", h, m);
  return str;
}

void GlobalInit() {
  irrecv.enableIRIn();
}

void GlobalLoop() {
  isIRRes = irrecv.decode(&irRes);
  if (isIRRes) {
    irrecv.resume();
  }
}

