int ButtonOnePin = 5;
int ButtonTwoPin = 4;

int LedOnePin = 2;
int LedTwoPin = 3;

int piezoPin = 7;

void ChangeState(int newState) {
  resultItem.state = currentState;
  resultItem.ticks = currentTime;

  WriteResult(resultItem);
  
  Serial.println("state: " + statusNames[(int)currentState]);
  Serial.print("result time: ");
  Serial.println(TimeToString(currentTime - loopTime));
  Serial.println("StatusWin: " + (String)resultItem.win);
  
  resultItem.win = false;
  resultItem.final = true;
  // TODO: запись прежнего состояния в память.

  // обновляем данные
  loopTime = currentTime;// - loopTime;
  currentState = (State) newState;
  PrintStatus(currentState);

  switch (currentState) {
    case one:
      digitalWrite(LedOnePin, HIGH);
      digitalWrite(LedTwoPin, LOW);
      break;
    case two:
      digitalWrite(LedOnePin, LOW);
      digitalWrite(LedTwoPin, HIGH);
      break;
    default:
      digitalWrite(LedOnePin, HIGH);
      digitalWrite(LedTwoPin, HIGH);
      break;
  }
}

void CheckSystem() {
  Serial.println("Checking system...");
  
  LcdPrint(0, 0, "Sechen' button");
  LcdPrint(0, 1, "Check system...");

  for (int i = 0; i < 2; i++)
  {
    digitalWrite(LedOnePin, HIGH);
    digitalWrite(LedTwoPin, HIGH);
    digitalWrite(piezoPin, HIGH);
    Serial.println("Lights on");
    delay(1000);
    digitalWrite(LedOnePin, LOW);
    digitalWrite(LedTwoPin, LOW);
    digitalWrite(piezoPin, LOW);
    Serial.println("Lights off");
    delay(1000);
  }

  LcdClear();

  ChangeState((int)neutral);
  isFirst = false;
}

void TimerLogicInit() {
  pinMode(LedOnePin, OUTPUT);
  pinMode(LedTwoPin, OUTPUT);
  pinMode(piezoPin, OUTPUT);
}

void TimerLogicLoop() {
  if (isFirst)
  {
    CheckSystem();
  } else {
    currentTime = millis();
    LcdPrint(3, 1, TimeToString(currentTime - loopTime));
    if ((digitalRead(ButtonOnePin) == HIGH) && (currentState != one))
    {
      ChangeState(one);
    } else
      if ((digitalRead(ButtonTwoPin) == HIGH) && (currentState != two))
      {
        ChangeState(two);
      }

    unsigned long timevalue = currentTime - loopTime;
    //Serial.println(timevalue);
    if ((timevalue >= maxTime) && (timevalue <= (maxTime + 2000)) && (currentState != neutral))
    {
      digitalWrite(piezoPin, HIGH);
      resultItem.win = true;
    } else {
      digitalWrite(piezoPin, LOW);
    }
  }
}

