#include <EEPROM.h>
#include <Arduino.h>

int MaxLengthEEPROM = EEPROM.length();

template <class T> int EEPROM_WriteAnything(int ee, const T& value)
{
  const byte* p = (const byte*)(const void*)&value;
  unsigned int i;
  for (i = 0; i<sizeof(value);i++)
    EEPROM.write(ee++, *p++);
  return i;
}

template <class T> int EEPROM_ReadAnything(int ee, T& value)
{
  byte* p = (byte*)(void*)&value;
  unsigned int i;
  for (i = 0; i<sizeof(value); i++)
    *p++ = EEPROM.read(ee++);
  return i;
}

void ClearMemory()
{
  int i = 0;
  for (i = 0; i < MaxLengthEEPROM; i++)
  {
    EEPROM[i] = 0;
  }
  //currentAddress = 0;
}

int CheckOverflowMemory(int curAddress, int sizeel)
{
  int result_address = curAddress;
  if ((curAddress + sizeel) >= MaxLengthEEPROM)
  {
    result_address = 0;
    Serial.println("CheckOverflowMemory: rollback to " + (String)curAddress);
    Serial.println("MaxEEPROM: " + (String)MaxLengthEEPROM);
  }
  return result_address;
}

int CheckMinusMemory(int curAddress, int sizeel)
{
  int result_address = curAddress - sizeel;
  if (result_address < 0)
  {
    result_address = MaxLengthEEPROM - sizeel;
    Serial.println("CheckMinusMemory: go to " + (String)result_address);
    Serial.println("MaxEEPROM: " + (String)MaxLengthEEPROM);
  }
  return result_address;
}

