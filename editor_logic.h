void changeEditState(MenuState newState) {
  menuState = newState;
  LcdClear();
  LcdPrint(0, 0, "Menu: ");
  LcdPrint(6, 0, menuNames[(int)menuState]);
}

void updateValue(int dir) {
  const int minutes_interval = 1;
  LcdClearLine(1);
  switch (menuState) {
    case timeedit:
      maxTime = maxTime + (60000 * minutes_interval * dir);
      if (maxTime < (minutes_interval * 60000))
        maxTime = minutes_interval * 60000;
      Serial.println(maxTime);
      break;
    default:
      int tmpValue = (int)teams[(int)menuState-1] + dir;
      if (tmpValue < 0)
        tmpValue = ColorHigh;
      if (tmpValue > ColorHigh)
        tmpValue = 0;
      teams[(int)menuState-1] = (Color) tmpValue;
      break;
  }
}

void refreshValue() {
  switch (menuState) {
    case timeedit:
      LcdPrint(0, 1, TimeEditToString(maxTime));
      break;
    default:
      LcdPrint(0, 1, colors[(int)teams[(int)menuState - 1]]);
      Serial.println((int)teams[(int)menuState - 1]);
      break;
  }
}

void EditorLogicInit() {
  
}

void EditorLogicLoop() {
  if (isIRRes) {
    if ((irRes.value == 0xff02fd) || (irRes.value == 0xff22dd))  {
      int tmpState = (int) menuState;
      switch (irRes.value) {
        case 0xff02fd:
          tmpState = tmpState + 1;
          if (tmpState > MenuStateHigh)
            tmpState = 0;
          break;
        case 0xff22dd:
          tmpState = tmpState - 1;
          if (tmpState < 0)
            tmpState = MenuStateHigh;
          break;
      }
      changeEditState((MenuState)tmpState);
    }
    changeEditState(menuState);
    if ((irRes.value == 0xffa857) || (irRes.value == 0xffe01f)) {
      switch (irRes.value) {
        case 0xffa857:
          updateValue(1);
          break;
        case 0xffe01f:
          updateValue(-1);
          break;
      }
    }

    refreshValue();
  }
}

