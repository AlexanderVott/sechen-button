#include <LiquidCrystal_I2C.h> // see by Frank de Brabander https://github.com/marcoschwartz/LiquidCrystal_I2C

LiquidCrystal_I2C lcd(0x27, 16, 2);

void LcdInit(bool enableBackLight) {
  lcd.init();
  
  if (enableBackLight) 
    lcd.backlight(); // подсветка дисплея
}

void LcdPrint(int column, int row, String text) {
  lcd.setCursor(column, row);
  lcd.print(text);
}

void LcdClearLine(int row) {
  lcd.setCursor(0, row);
  for (int i = 0; i < 16; i++)
    lcd.print(" ");
}

void LcdClear() {
  LcdClearLine(0);
  LcdClearLine(1);
}

